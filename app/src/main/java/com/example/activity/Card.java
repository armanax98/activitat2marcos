package com.example.activity;

public class Card {
    public enum Estat {BACK, FRONT, FIXED}
    private int frontImage;
    private int backImage;
    private Estat estat;

    public Card(int backImage, int frontImage){
        this.backImage = backImage;
        this.frontImage = frontImage;
        this.estat = Estat.BACK;
    }
    public int getFrontImage(){
        return frontImage;
    }

    public void setEstat(Estat estat) {
        this.estat = estat;
    }

    public Estat getEstat() {
        return estat;
    }





    public int getImage(){
        int image = 0;
        if (getEstat() == Estat.BACK){
            image = backImage;
        } else if (getEstat() == Estat.FRONT){
            image = frontImage;

        }
        return image;
    }

    public void girar(){
        if (getEstat()== Estat.BACK){
            setEstat(Estat.FRONT);
        } else if (getEstat() == Estat.FRONT){
            setEstat(Estat.BACK);
        }
    }



}
