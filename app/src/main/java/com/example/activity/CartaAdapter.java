package com.example.activity;

import android.content.Context;
import android.media.Image;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class CartaAdapter {
    private Context context;
    private ArrayList<Card> listCard;
    private Card card1 = null;
    private Card card = null;
    private boolean onGoing = false;
    private int aciertos;
    private boolean start = true;
    private TextView text;
    private int time = 30;
    private CountDownTimer cdt;

    public CartaAdapter(Context context, ArrayList<Card> listCard, TextView text){
        this.context = context;
        this.listCard = listCard;
        this.text = text;
        text.setText(String.valueOf(time));
    }

    public class CardsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public ImageView imagen;
        public CardsViewHolder(View v){

        }
    }

    @Override

    public void onClick(View view){
        if (start){
            start=false;
            cdt = new CountDownTimer() {
                @Override
                public void onTick(long millisUntilFinished) {
                    time--;
                    text.setText(String.valueOf(time));
                }

                @Override
                public void onFinish() {
                    Toast.makeText(context, text "GAME OVER", Toast.LENGTH_SHORT).show();
                    onGoing = true;
                }
            }.start();
        }
        if (!onGoing){
            card = listCard.get(getAdapterPosition());
            switch (card.getEstat()){
                case FRONT:
                    card.girar();
                    card1 = null;
                    notifyDataSetChanged();
                    break;
                case BACK:
                    card1.girar();
                    notifyDataSetChanged();
                    break;}
            }else{
            card.girar();
            notifyDataSetChanged();
            if (card1.getFrontImage()== card.getFrontImage()){
                card1.setEstat(Card.Estat.FIXED);
                card.setEstat(Card.Estat.FIXED);
                card1 = null;
                aciertos++;
                if (aciertos == (listCard.size()/2)){
                    Toast.makeText(context, text "HAS GUANYAT");
                    onGoing = true;
                    cdt.cancel();
                }
            }
            }
        }

    private int getAdapterPosition() {
        int position = 0;
        return position;
    }

    private void notifyDataSetChanged() {
        Card.Estat beforeData = card.getEstat();
        if (Card.Estat.FRONT == beforeData){
            System.out.println("No changed");
        } else if (Card.Estat.FRONT!= beforeData){
            System.out.println("Changed");
        }

        if (Card.Estat.BACK == beforeData){
            System.out.println("Changed");
        } else if (Card.Estat.BACK!= beforeData){
            System.out.println("No changed");
        }

        if (Card.Estat.FIXED == beforeData){
            System.out.println("Changed");
        } else if (Card.Estat.FIXED!= beforeData){
            System.out.println("No changed");
        }

    }
}


